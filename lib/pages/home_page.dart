import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:go_securi/common/equipement.dart';
import 'package:go_securi/pages/widgets/equipment_item.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;

import '../services/requests.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  HomePageState createState() => HomePageState();
}

class HomePageState extends State<HomePage> {
  final equipmentController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  late List<String> equipments = [];
  late List<String> equipmentItems = [];
  List<Equipement> equipmentsList = [];
  late String responseBody = "";

  @override
  void initState() {
    super.initState();
  }

  Future<http.Response> fetchEquipment() async {
    final response = await Requests().getList("input%2Fliste");
    if (response.statusCode == 200) {
      responseBody = response.body;
    } else if (kDebugMode) {
      print("ERROR: ${response.body}");
    }
    return response;
  }

  Future<List<Equipement>> _getEquipments() async {
    var equipJson = [];
    var data = await fetchEquipment();

    equipments = data.body.split("\n");
    for (var item in equipments) {
      equipmentItems.add(item.split(" ").sublist(1).join(" "));
    }

    var resBody = {};
    String equipment = "{\"equipment\": [X]}";
    var equipTemp = "";
    String str = "";

    for (var item in equipmentItems) {
      resBody["name"] = item;
      str = jsonEncode(resBody);
      equipTemp = equipTemp + "," + str;
    }
    equipTemp = equipTemp.replaceFirst(",", "");
    equipment = equipment.replaceFirst("X", equipTemp);

    equipJson = json.decode(equipment)["equipment"];

    for (var e in equipJson) {
      Equipement equipItem = Equipement(e["name"]);
      equipmentsList.add(equipItem);
    }
    return equipmentsList;
  }

  Future<http.Response> addEquipment(String equipment) async {
    const pathFile = "input%2Fliste";
    final response =
        await Requests().addEquipmentToList(pathFile, responseBody, equipment);
    if (response.statusCode == 200) {
      responseBody = response.body;
      setState(() => {equipmentItems.length});
    } else if (kDebugMode) {
      print("ERROR: ${response.body}");
    }
    return response;
  }

  @override
  Widget build(BuildContext context) {
    setState(() {
      equipmentItems.clear();
    });

    return Scaffold(
      body: Container(
        alignment: Alignment.center,
        child: Column(
          children: [
            Container(
              padding:
                  const EdgeInsets.only(left: 30.0, top: 20.0, bottom: 10.0),
              alignment: Alignment.centerLeft,
              child: Text(
                "Equipements",
                style: GoogleFonts.roboto(
                    textStyle: const TextStyle(
                        fontSize: 30.0, fontWeight: FontWeight.w500)),
              ),
            ),
            const Divider(),
            Expanded(
              child: FutureBuilder(
                future: _getEquipments(),
                builder: (context, AsyncSnapshot snapshot) {
                  if (snapshot.connectionState == ConnectionState.none &&
                      !snapshot.hasData) {
                    return Container();
                  }

                  return ListView.builder(
                      shrinkWrap: true,
                      itemCount: equipmentItems.length,
                      itemBuilder: (BuildContext ctxt, int index) {
                        if (index < equipments.length) {
                          return EquipmentItem(item: equipmentItems[index]);
                        }
                        return Container();
                      });
                },
              ),
            ),
            Form(
              key: _formKey,
              child: Container(
                padding: const EdgeInsets.only(top: 10.0, bottom: 10.0),
                color: Colors.grey[100],
                child: Container(
                  margin: const EdgeInsets.only(left: 15.0, right: 15.0),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(25.0)),
                  child: Row(
                    children: [
                      SizedBox(width: MediaQuery.of(context).size.width * 0.05),
                      SizedBox(
                        width: MediaQuery.of(context).size.width * 0.73,
                        child: TextFormField(
                          controller: equipmentController,
                          style: GoogleFonts.roboto(),
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: "add an equipment...",
                            hintStyle:
                                GoogleFonts.roboto(fontWeight: FontWeight.w300),
                          ),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Saisir un équipement';
                            }
                            return null;
                          },
                        ),
                      ),
                      SizedBox(
                          width: MediaQuery.of(context).size.width * 0.035),
                      ClipOval(
                        child: Material(
                          color: const Color(0xFF659224),
                          child: InkWell(
                            onTap: () {
                              if (_formKey.currentState!.validate()) {
                                String equipmentParse =
                                    "${equipmentController.text} ";
                                List<String> splitEquipment =
                                    equipmentParse.split(" ");
                                String equipmentFormat =
                                    "${splitEquipment[0].toLowerCase()} ${equipmentController.text}";

                                addEquipment(equipmentFormat);

                                ScaffoldMessenger.of(context).showSnackBar(
                                  const SnackBar(
                                      content: Text('Envoi en cours...')),
                                );
                              } else if (kDebugMode) {
                                print("ERROR valid");
                              }
                            },
                            child: const SizedBox(
                              width: 40,
                              height: 40,
                              child: Icon(
                                Icons.add,
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
