import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class EquipmentItem extends StatelessWidget {
  final String item;
  const EquipmentItem({
    Key? key,
    required this.item,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      const SizedBox(height: 20.0),
      Container(
        padding: const EdgeInsets.only(left: 30.0),
        child: Text(
          item,
          style: GoogleFonts.roboto(
            textStyle: const TextStyle(fontSize: 20.0),
            fontWeight: FontWeight.w300
          ),
        ),
      ),
      const SizedBox(height: 20.0),
      const Divider()
    ]);
  }
}
