import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class CustomButton extends StatelessWidget {
  final Function() onPressed;
  final String text;
  const CustomButton({Key? key, required this.onPressed, required this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const SizedBox(height: 20.0),
        ElevatedButton(
            style: ButtonStyle(
              backgroundColor:
                  MaterialStateProperty.all<Color>(const Color(0xFF379EC1)),
              shape: MaterialStateProperty.resolveWith<OutlinedBorder>((_) =>
                  RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30.0))),
            ),
            onPressed: onPressed,
            child: Container(
              padding: const EdgeInsets.all(15.0),
              child: Text(
                text,
                style: GoogleFonts.roboto(
                    textStyle: const TextStyle(
                        fontSize: 25.0, fontWeight: FontWeight.w500)),
              ),
            )),
      ],
    );
  }
}
