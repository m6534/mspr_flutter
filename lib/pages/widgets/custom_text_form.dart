import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class CustomTextForm extends StatelessWidget {
  final String hintText;
  final TextInputType? keyboardType;
  final String? Function(String?) validator;
  final bool password;
  const CustomTextForm(
      {Key? key,
      required this.hintText,
      this.keyboardType,
      required this.password,
      required this.validator})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const SizedBox(height: 20.0),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 30.0),
          child: Container(
            padding: const EdgeInsets.only(left: 25.0),
            decoration: BoxDecoration(
              color: Colors.grey[300],
              borderRadius: BorderRadius.circular(25.0),
            ),
            child: TextFormField(
                style: GoogleFonts.ubuntu(
                  textStyle: const TextStyle(color: Color(0xFF379EC1)),
                  fontWeight: FontWeight.w300
                ),
                obscureText: password,
                keyboardType: keyboardType,
                decoration: InputDecoration(
                  hintText: hintText,
                  hintStyle: GoogleFonts.roboto(fontWeight: FontWeight.w300),
                  border: InputBorder.none,
                ),
                validator: validator),
          ),
        ),
      ],
    );
  }
}
