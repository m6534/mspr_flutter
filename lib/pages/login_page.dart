import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:go_securi/pages/widgets/custom_button.dart';
import 'package:go_securi/pages/widgets/custom_text_form.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;

import '../services/requests.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  LoginPageState createState() => LoginPageState();
}

class LoginPageState extends State<LoginPage> {
  late String responseBody = "";
  late String tmpLogin = "";
  late String tmpPasswd = "";
  late List<String> staffNameList = [];
  late List<String> staffPswList = [];

  @override
  void initState() {
    super.initState();
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual, overlays: [
      SystemUiOverlay.bottom,
    ]);
  }

  Future<http.Response> fetchStaffName() async {
    final response = await Requests().getList("input%2Fstaff");
    if (response.statusCode == 200) {
      responseBody = response.body;
    } else if (kDebugMode) {
      print("ERROR: ${response.body}");
    }
    return response;
  }

  Future<http.Response> fetchStaffPassword(String login) async {
    final response = await Requests().getList("input%2F$login");
    if (response.statusCode == 200) {
      responseBody = response.body;
    } else if (kDebugMode) {
      print("ERROR: ${response.body}");
    }
    return response;
  }

  Future<List<String>> _getListStaffName() async {
    var data = await fetchStaffName();
    staffNameList = data.body.split("\n");

    return staffNameList;
  }

  Future<String> _getListStaffPassword(String login) async {
    var data = await fetchStaffPassword(login);
    staffPswList = data.body.split("\n");
    tmpPasswd = staffPswList[3];
    return staffPswList[3];
  }

  @override
  Widget build(BuildContext context) {
    final _formKey = GlobalKey<FormState>();
    setState(() {
      tmpLogin = "";
    });

    _getListStaffName();
    return Scaffold(
      backgroundColor: Colors.grey[100],
      body: Form(
        autovalidateMode: AutovalidateMode.always,
        key: _formKey,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              width: 100.0,
              child: Image.asset("assets/images/go_securi_logo.png"),
            ),
            Container(
              alignment: Alignment.center,
              child: Text(
                "GO Securi",
                style: GoogleFonts.roboto(
                    textStyle: const TextStyle(
                        fontSize: 40.0,
                        fontWeight: FontWeight.w500,
                        color: Color(0xFF659224))),
              ),
            ),
            CustomTextForm(
                password: false,
                hintText: "username",
                validator: (value) {
                  // If the input is empty
                  if (value == null || value.isEmpty) {
                    return "Veuillez saisir un nom d'utilisateur valide";
                  }

                  // Get the correct staff name if it exist
                  for (var item in staffNameList) {
                    if (item == value) {
                      tmpLogin = value;
                    }
                  }
                  return tmpLogin == "" ? "Nom d'utilisateur inconnu" : null;
                }),
            CustomTextForm(
                password: true,
                hintText: "password",
                validator: (value) {
                  // If the input is empty
                  if (value == null || value.isEmpty) {
                    return "Veuillez saisir un mot de passe valide";
                  }

                  // If just password is put
                  if (tmpLogin != "") {
                    _getListStaffPassword(tmpLogin);
                  }

                  // If it's not the good password
                  if (value != tmpPasswd) {
                    return "Le mot de passe ne correspond pas avec le nom";
                  }
                  return null;
                }),
            CustomButton(
              text: "Se connecter",
              onPressed: () {
                if (_formKey.currentState!.validate()) {
                  ScaffoldMessenger.of(context).showSnackBar(
                    const SnackBar(content: Text('Processing Data')),
                  );

                  // Clear form after a connection
                  setState(() {});
                  Navigator.pushNamed(context, "/home");
                }
              },
            ),
          ],
        ),
      ),
    );
  }
}
