import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;

class Requests {
  String apiKey = "Bearer glpat-NkVp3MybERF3ZWYeQTQM";

  // Get list in GitLab
  Future<http.Response> getList(String pathFile) async {
    final response = await http.get(
        Uri(
          host: 'gitlab.com',
          scheme: 'https',
          path:
              'api/v4/projects/31957048/repository/files/$pathFile%2Etxt/raw',
        ),
        headers: {
          HttpHeaders.authorizationHeader: apiKey
        });
    return response;
  }

  // Push an equipment into an existing GitLab list
  Future<http.Response> addEquipmentToList(String pathFile, String responseBody, String equipment) async {
    final response = await http.put(
      Uri(
        host: 'gitlab.com',
        scheme: 'https',
        path: 'api/v4/projects/31957048/repository/files/$pathFile%2Etxt',
      ),
      headers: <String, String>{
        'Content-Type': 'application/json',
        'Authorization': apiKey
      },
      body: jsonEncode(<String, String>{
        "branch": "main",
        "content": "$responseBody\n$equipment",
        "commit_message": "add ${equipment.split(" ")[0]}"
      }),
    );
    return response;
  }
}
