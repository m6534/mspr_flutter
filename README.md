# GoSecuri Flutter App

## About

The company currently has equipment management software which data is saved on a Git repository in text format.
This repository also stores the list of agents and scans of their IDs.

An app solution makes it possible to consult equipments in the company.
This application also allows you to add new equipment to the list.

Its content can be viewed on any smartphone connected to the Internet.

## Description

Mobile application for viewing and adding equipment present in the company.

The application uses the following files as input: 
- agent id list `staff.txt`
- equipment list `list.txt`

These files are available in this directory : https://gitlab.com/m6534/mspr_java

## Usage

- Prerequisite:
  - Compatible for `Android` and `IOS`
  - Have a GoSecuri account
